
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.clock import mainthread, Clock
from kivy.uix.boxlayout import BoxLayout

from plyer import gps

from math import sin, cos
from random import uniform


DEG_LAT = 40008552.0 / 360.0
DEG_LON_EQUATOR = 40075696.0 / 360.0
deg_lon = lambda lat: cos(lat) * DEG_LON_EQUATOR


def dist(gps1, gps2):
    dlat = (gps1['lat'] - gps2['lat']) * DEG_LAT
    dlon = (gps1['lon'] - gps2['lon']) * deg_lon(gps1['lat'])
    d = (dlat ** 2 + dlon ** 2) ** 0.5
    return round(d)

def random_near_point(gps):
    random_angle = uniform(-180, 180)
    random_dist = uniform(800, 1200)
    dlat = random_dist * sin(random_angle) / DEG_LAT
    dlon = random_dist * cos(random_angle) / deg_lon(gps.get('lat', 0))
    res = gps.copy()
    res['lat'] += dlat
    res['lon'] += dlon
    return res


class Main(App):

    def __init__(self):
        super(Main, self).__init__()
        self.root = BoxLayout()
        self.label = Label()
        self.button = Button(text="Начать новую игру")
        self.button.bind(on_press=self.start_game)
        self.gps = {'lat': 0, 'lon': 0}
        self.target_gps = {'lat': 0, 'lon': 0}
        self.game_started = False

    def build(self):
        self.root.orientation = 'vertical'
        self.root.add_widget(self.label, 0)
        self.root.add_widget(self.button, 0)
        try:
            gps.configure(on_location=self.on_location)
        except NotImplementedError:
            pass
        return self.root

    def start_game(self, _):
        self.target_gps = random_near_point(self.gps)
        self.game_started = True
        # TODO: Вывод надписи сразу

    @mainthread
    def on_location(self, **kwargs):
        self.gps = kwargs
        if self.game_started:
            self.label.text = str(dist(self.gps, self.target_gps))

    def test_loop(self, _):
        self.target_gps = random_near_point(self.gps)
        self.label.text = str(dist(self.gps, self.target_gps))

    def on_start(self):
        self.label.text = 'test'
        try:
            gps.start(1000, 0)
        except NotImplementedError:
            Clock.schedule_interval(self.test_loop, 1)


if __name__ == '__main__':
    app = Main()
    app.run()